package com.shakuro.contributorstest.db

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(users: List<User>)

    @Query("SELECT * FROM users ORDER BY id ASC")
    fun allUsers(): PagingSource<Int, User>

    @Query("SELECT * FROM users WHERE id == :id ORDER BY id ASC")
    suspend fun user(id: Int): User

    @Query("DELETE FROM users")
    suspend fun deleteAll()

    @Query("SELECT COALESCE(MAX(id), 0) FROM users")
    suspend fun getNextId(): Long
}
