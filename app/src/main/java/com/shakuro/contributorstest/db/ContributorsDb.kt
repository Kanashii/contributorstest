package com.shakuro.contributorstest.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(
    entities = [User::class],
    version = 1,
    exportSchema = false
)
abstract class UsersDb : RoomDatabase() {
    companion object {
        fun create(context: Context): UsersDb {
            return  Room.databaseBuilder(context, UsersDb::class.java, "users.db")
                .fallbackToDestructiveMigration()
                .build()
        }
    }

    abstract fun users(): UserDao
}