package com.shakuro.contributorstest.repo

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.shakuro.contributorstest.api.UsersApi
import com.shakuro.contributorstest.db.User
import com.shakuro.contributorstest.db.UsersDb

@ExperimentalPagingApi
class UsersMediator(
    private val db: UsersDb,
    private val api: UsersApi
) : RemoteMediator<Int, User>() {
    override suspend fun load(loadType: LoadType, state: PagingState<Int, User>): MediatorResult {
        if (loadType == LoadType.PREPEND) {
            return MediatorResult.Success(endOfPaginationReached = true)
        }
        val users = api.getUsers(db.users().getNextId())
        db.withTransaction { db.users().insert(users) }
        return MediatorResult.Success(endOfPaginationReached = users.isEmpty())
    }

}