package com.shakuro.contributorstest.repo

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.shakuro.contributorstest.api.UsersApi
import com.shakuro.contributorstest.db.User
import com.shakuro.contributorstest.db.UsersDb
import kotlinx.coroutines.flow.Flow

class DbUsersRepo(
    private val db: UsersDb,
    private val api: UsersApi
) {
    @ExperimentalPagingApi
    fun getUsers(): Flow<PagingData<User>> =
        Pager(config = PagingConfig(pageSize = 30), remoteMediator = UsersMediator(db, api)) {
            db.users().allUsers()
        }.flow

    suspend fun getUser(id: Int): User = db.users().user(id)
}