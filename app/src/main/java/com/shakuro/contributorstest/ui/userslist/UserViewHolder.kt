package com.shakuro.contributorstest.ui.userslist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.shakuro.contributorstest.R
import com.shakuro.contributorstest.db.User
import com.squareup.picasso.Picasso

class UserViewHolder(
    view: View,
    private val picasso: Picasso,
    userSelectedListener: (User) -> Unit
) : RecyclerView.ViewHolder(view) {
    private val login: TextView = view.findViewById(R.id.userLogin)
    private val id: TextView = view.findViewById(R.id.userId)
    private val avatar: ImageView = view.findViewById(R.id.userAvatar)
    private var user: User? = null

    init {
        view.setOnClickListener {
            user?.let {
                userSelectedListener(it)
            }
        }
    }

    fun bind(user: User?) {
        this.user = user
        login.text = user?.login
        id.text = user?.id?.toString()
        user?.avatarUrl?.let {
            picasso.load(it)
                .into(avatar)
        }

    }

    companion object {
        fun create(
            parent: ViewGroup,
            picasso: Picasso,
            userSelectedListener: (User) -> Unit
        ): UserViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.user_item, parent, false)
            return UserViewHolder(view, picasso, userSelectedListener)
        }
    }
}