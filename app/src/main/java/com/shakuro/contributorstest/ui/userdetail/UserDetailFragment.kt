package com.shakuro.contributorstest.ui.userdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.shakuro.contributorstest.R
import com.shakuro.contributorstest.api.UsersApi
import com.shakuro.contributorstest.db.UsersDb
import com.shakuro.contributorstest.repo.DbUsersRepo
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.user_details_fragment.*

class UserDetailFragment : Fragment() {

    companion object {
        const val USER_ID = "USER_ID"
    }

    private val userId: Int by lazy {
        arguments?.getInt(USER_ID)
            ?: throw IllegalArgumentException("USER_ID must be passed to UserDetailFragment")
    }

    @Suppress("UNCHECKED_CAST")
    private val viewModel: UserDetailViewModel by viewModels {
        object : AbstractSavedStateViewModelFactory(this, null) {
            override fun <T : ViewModel?> create(
                key: String,
                modelClass: Class<T>,
                handle: SavedStateHandle
            ): T {
                val repo = DbUsersRepo(
                    UsersDb.create(requireContext()),
                    UsersApi.create()
                )
                return UserDetailViewModel(repo, userId) as T
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.user_details_fragment, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.user.observe(viewLifecycleOwner, Observer {
            userLogin.text = it.login
            Picasso.get()
                .load(it.avatarUrl)
                .into(userAvatar)
        })
    }
}