package com.shakuro.contributorstest.ui.userslist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadState
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.shakuro.contributorstest.R
import com.shakuro.contributorstest.api.UsersApi
import com.shakuro.contributorstest.db.UsersDb
import com.shakuro.contributorstest.repo.DbUsersRepo
import kotlinx.android.synthetic.main.users_list_fragment.*
import kotlinx.coroutines.flow.collectLatest

@ExperimentalPagingApi
class UsersListFragment : Fragment() {

    private lateinit var adapter: UsersAdapter

    @Suppress("UNCHECKED_CAST")
    private val viewModel: UsersListViewModel by viewModels {
        object : AbstractSavedStateViewModelFactory(this, null) {
            override fun <T : ViewModel?> create(
                key: String,
                modelClass: Class<T>,
                handle: SavedStateHandle
            ): T {
                val repo = DbUsersRepo(
                    UsersDb.create(requireContext()),
                    UsersApi.create()
                )
                return UsersListViewModel(repo) as T
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.users_list_fragment, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        adapter = UsersAdapter { findNavController().navigate(UsersListFragmentDirections.usersListToUserDetails(it.id))}
        usersList.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        usersList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        usersList.adapter = adapter
        lifecycleScope.launchWhenCreated {
            viewModel.users.collectLatest {
                adapter.submitData(it)
            }
        }
        lifecycleScope.launchWhenCreated {
            adapter.loadStateFlow.collectLatest { loadStates ->
                swipeRefresh.isRefreshing = loadStates.refresh is LoadState.Loading
            }
        }
        swipeRefresh.setOnRefreshListener { adapter.refresh() }
    }
}