package com.shakuro.contributorstest.ui.userslist

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.shakuro.contributorstest.db.User
import com.squareup.picasso.Picasso

class UsersAdapter(private val userSelectedListener: (User) -> Unit)
    : PagingDataAdapter<User, UserViewHolder>(USER_COMPARATOR) {

    private val picasso: Picasso by lazy {
        Picasso.get()
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder.create(parent, picasso, userSelectedListener)
    }

    companion object {
        val USER_COMPARATOR = object : DiffUtil.ItemCallback<User>() {
            override fun areContentsTheSame(oldItem: User, newItem: User): Boolean =
                oldItem.id == newItem.id

            override fun areItemsTheSame(oldItem: User, newItem: User): Boolean =
                oldItem.id == newItem.id
        }
    }
}
