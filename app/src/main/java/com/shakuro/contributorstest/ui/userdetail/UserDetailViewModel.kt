package com.shakuro.contributorstest.ui.userdetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.shakuro.contributorstest.db.User
import com.shakuro.contributorstest.repo.DbUsersRepo

class UserDetailViewModel(
    private val repo: DbUsersRepo,
    private val userId: Int
) : ViewModel() {

    val user: LiveData<User> = liveData {
        emit(repo.getUser(userId))
    }
}