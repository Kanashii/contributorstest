package com.shakuro.contributorstest.ui.userslist

import androidx.lifecycle.ViewModel
import androidx.paging.ExperimentalPagingApi
import com.shakuro.contributorstest.repo.DbUsersRepo

class UsersListViewModel(
    repo: DbUsersRepo
) : ViewModel() {
    @ExperimentalPagingApi
    val users = repo.getUsers()
}