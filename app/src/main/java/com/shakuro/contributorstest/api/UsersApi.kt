package com.shakuro.contributorstest.api

import android.util.Log
import com.shakuro.contributorstest.BuildConfig
import com.shakuro.contributorstest.db.User
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface UsersApi {

    @GET("/users")
    suspend fun getUsers(@Query("since") since: Long): List<User>

    companion object {
        private const val BASE_URL = "https://api.github.com/"
        fun create(): UsersApi {
            val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { Log.d("API", it) })
            logger.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()
            return Retrofit.Builder()
                .baseUrl(HttpUrl.parse(BASE_URL)!!)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(UsersApi::class.java)
        }
    }
}